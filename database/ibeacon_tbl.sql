-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.8-MariaDB
-- PHP のバージョン: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `ibeacon_tbl`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `my_beacon`
--

CREATE TABLE `my_beacon` (
  `id` int(11) NOT NULL,
  `beacon_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `my_beacon`
--

INSERT INTO `my_beacon` (`id`, `beacon_name`) VALUES
(1, 'ducgiang'),
(2, 'ducgiang'),
(3, 'ducgiang');

-- --------------------------------------------------------

--
-- テーブルの構造 `my_beacon_room_relation`
--

CREATE TABLE `my_beacon_room_relation` (
  `id` int(11) NOT NULL,
  `beacon_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `my_beacon_room_relation`
--

INSERT INTO `my_beacon_room_relation` (`id`, `beacon_id`, `room_id`) VALUES
(1, 2, 3);

-- --------------------------------------------------------

--
-- テーブルの構造 `my_beacon_rssi`
--

CREATE TABLE `my_beacon_rssi` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `beacon_id` int(11) NOT NULL,
  `micro_id` int(11) NOT NULL,
  `near_rssi` int(11) NOT NULL,
  `center_rssi` int(11) NOT NULL,
  `far_rssi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `my_micro`
--

CREATE TABLE `my_micro` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `micro_name` varchar(30) NOT NULL,
  `coordinate_x` int(11) NOT NULL,
  `coordinate_y` int(11) NOT NULL,
  `coordinate_z` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `my_micro`
--

INSERT INTO `my_micro` (`id`, `room_id`, `micro_name`, `coordinate_x`, `coordinate_y`, `coordinate_z`) VALUES
(1, 0, 'trangiang', 200, 250, 300),
(2, 0, 'trangiang', 0, 0, 0),
(3, 0, 'trangiang', 0, 0, 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `my_room`
--

CREATE TABLE `my_room` (
  `id` int(11) NOT NULL,
  `room_name` varchar(30) NOT NULL,
  `coordinate_x` int(11) NOT NULL,
  `coordinate_y` int(11) NOT NULL,
  `coordinate_z` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `my_room`
--

INSERT INTO `my_room` (`id`, `room_name`, `coordinate_x`, `coordinate_y`, `coordinate_z`) VALUES
(1, 'duc', 170, 200, 240),
(2, 'leduc', 200, 250, 300),
(3, 'leduc', 200, 250, 300),
(4, 'leduc', 200, 250, 300),
(5, 'leduc', 200, 250, 300);

-- --------------------------------------------------------

--
-- テーブルの構造 `my_rssi`
--

CREATE TABLE `my_rssi` (
  `id` int(11) NOT NULL,
  `beacon_id` int(11) NOT NULL,
  `micro_id` int(11) NOT NULL,
  `rssi` int(11) NOT NULL,
  `date_time` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `my_beacon`
--
ALTER TABLE `my_beacon`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `my_beacon_room_relation`
--
ALTER TABLE `my_beacon_room_relation`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `my_beacon_rssi`
--
ALTER TABLE `my_beacon_rssi`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `my_micro`
--
ALTER TABLE `my_micro`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `my_room`
--
ALTER TABLE `my_room`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `my_rssi`
--
ALTER TABLE `my_rssi`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `my_beacon`
--
ALTER TABLE `my_beacon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- テーブルのAUTO_INCREMENT `my_beacon_room_relation`
--
ALTER TABLE `my_beacon_room_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- テーブルのAUTO_INCREMENT `my_beacon_rssi`
--
ALTER TABLE `my_beacon_rssi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- テーブルのAUTO_INCREMENT `my_micro`
--
ALTER TABLE `my_micro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- テーブルのAUTO_INCREMENT `my_room`
--
ALTER TABLE `my_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- テーブルのAUTO_INCREMENT `my_rssi`
--
ALTER TABLE `my_rssi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
