<?php
    $serverName = "localhost";
    $userName   = "root";
    $password   = "leduc";
    $table      = "ibeacon_tbl";

    $conn = mysqli_connect($serverName, $userName, $password, $table);
    if (!$conn) {
        die('Connection failed ' . mysqli_error($conn));
        exit();
    }
    
    if (isset($_POST["btn_submit"])) {
        // lấy thông tin người dùng
        $room_name      = $_POST["room_name"];
        $length_of_room = $_POST["length_of_room"];
        $width_of_room  = $_POST["width_of_room"];
        $height_of_room = $_POST["height_of_room"];
        $beacon_name    = $_POST["beacon_name"];
        $select_beacon_id  = $_POST["select_beacon_id"];
        $select_room_id    = $_POST["select_room_id"];

        if ($room_name == "" || $length_of_room <= 0 || $width_of_room <= 0 || $height_of_room <= 0 ||
            $beacon_name == "") {
            echo "全部入力してください。";
        } else {
            // lay id nhan duoc ktra trong csdl roi lay ra ten
            $beacon_id = "SELECT beacon_name FROM my_beacon where id = " . $select_beacon_id;
            $room_id = "SELECT room_name FROM my_room where id = " . $select_room_id;
            $result_beacon_name = $conn->query($beacon_id);
            $result_room_name = $conn->query($room_id);
            if ($result_beacon_name->num_rows > 0) {
                $row = $result_beacon_name->fetch_assoc());
                echo $row["beacon_name"] . "<br/>";
            }
            if ($result_room_name->num_rows > 0) {
                $row = $result_room_name->fetch_assoc());
                    echo $row["room_name"] . "<br/>";
            }
            
            // 時間を取得する
            date_default_timezone_set('japan');
            $date_time = date("Y/m/d G:i:s");
 
            //create_room($room_name, $length_of_room, $width_of_room, $height_of_room);
            //create_micro(1, 'abc', -1.5, 1, 1);
            //create_beacon_room_relation(5, 6);
            //create_beacon_rssi(4, 5, 6, 'aaa', -60);
            //create_rssi(3, 4, -69, $date_time);

            //　ビーコン取得
            take_data_beacon('trangiang123');

            // lay duoc ten ma khi minh chon
            //echo $select_beacon . "<br/>";

            //close database
            $conn->close();
        }
    }

    //ビーコン取得(lay du lieu beacon)
    function take_data_beacon($micro_name) {
        // グローバル変数を定義
        GLOBAL $conn;
        // DBからマイコン名で部屋IDを探す。
        $micro_name_sql = "SELECT room_id FROM my_micro where micro_name = '$micro_name'";
        $result_room_id = $conn->query($micro_name_sql);
        if (!$result_room_id) {
            echo "error" . $micro_name_sql . "<br>" . $conn->error . "<br/>";
            exit();
        }
        // 取ってきた部屋IDを代入する。
        $row_room_id = $result_room_id->fetch_assoc();
        $room_id = $row_room_id["room_id"];
        
        // DBから部屋IDでビーコンIDを探す。
        $room_id_sql = "SELECT beacon_id FROM my_beacon_room_relation where room_id = $room_id"; // 1 2 3
        $result_beacon_id = $conn->query($room_id_sql);
        if(!$result_beacon_id) {
            echo "error" . $room_id_sql . "<br>" . $conn->error . "<br/>";
            exit();
        }
        // 複数のビーコンIDがあるからループで回す。
        while($row_beacon_id = $result_beacon_id->fetch_assoc()) {
            $beacon_id = $row_beacon_id["beacon_id"] . "<br/>";
            // DBから複数のビーコンIDでビーコン名を探す。
            $beacon_id_sql = "SELECT beacon_name FROM my_beacon where id = '$beacon_id'";
            $result_beacon_name = $conn->query($beacon_id_sql);
            if(!$result_beacon_name) {
                echo "error " . $beacon_id_sql . "<br>" . $conn->error . "<br/>";
                exit();
            }
            // 複数のビーコン名があるからループで回す。
            while($row_beacon_name = $result_beacon_name->fetch_assoc()) {
                echo $row_beacon_name["beacon_name"] . "<br/>";
            }
        }
    }

    // 部屋登録
    function create_room($room_name, $length_of_room, $width_of_room, $height_of_room) {
        // グローバル変数を定義
        GLOBAL $conn;
        $room_sql = "INSERT INTO my_room (id, room_name, length_of_room, width_of_room, height_of_room)
                    VALUES (null, '$room_name', '$length_of_room', '$width_of_room', '$height_of_room')";
        if($conn->query($room_sql) === true) {
            echo "isnert successful <br/>";
        } else {
            echo "Error: " . $room_sql . "<br>" . $conn->error . "<br/>";
        }
    }

    // マイコン登録
    function create_micro($room_id, $micro_name, $coordinate_x, $coordinate_y, $coordinate_z) {
        // グローバル変数を定義
        GLOBAL $conn;
        $micro_sql = "INSERT INTO my_micro (id, room_id, micro_name, coordinate_x, coordinate_y, coordinate_z)
                    VALUES (null, '$room_id', '$micro_name', '$coordinate_x', '$coordinate_y', '$coordinate_z')";
        if($conn->query($micro_sql) === true) {
            echo "isnert successful <br/>";
        } else {
            echo "Error: " . $micro_sql . "<br>" . $conn->error . "<br/>";
        }
    }
    
    // ビーコン登録
    function create_beacon($beacon_name) {
        // グローバル変数を定義
        GLOBAL $conn;
        $beacon_sql = "INSERT INTO my_micro (id, beacon_name) VALUES (null, '$beacon_name')";
        if($conn->query($beacon_sql) === true) {
            echo "isnert successful <br/>";
        } else {
            echo "Error: " . $beacon_sql . "<br>" . $conn->error . "<br/>";
        }
    }

    // ビーコンと部屋の関連付け登録
    function create_beacon_room_relation($beacon_id, $room_id) {
        // グローバル変数を定義
        GLOBAL $conn;
        $beacon_room_sql = "INSERT INTO my_beacon_room_relation (id, beacon_id, room_id) VALUES (null, '$beacon_id', '$room_id')";
        if($conn->query($beacon_room_sql) === true) {
            echo "isnert successful <br/>";
        } else {
            echo "Error: " . $beacon_room_sql . "<br>" . $conn->error . "<br/>";
        }
    }

    // ビーコンRSSI範囲登録
    function create_beacon_rssi($room_id, $beacon_id, $micro_id, $distance, $beacon_rssi) {
        // グローバル変数を定義
        GLOBAL $conn;
        $beacon_rssi_sql = "INSERT INTO my_beacon_rssi (id, room_id, beacon_id, micro_id, distance, beacon_rssi)
                    VALUES (null, '$room_id', '$beacon_id', '$micro_id', '$distance', '$beacon_rssi')";
        /*$beacon_rssi_sql = "INSERT INTO my_beacon_rssi (id, room_id, beacon_id, micro_id, range, beacon_rssi)
                    VALUES (?, ?, ?, ?, ?, ?)";*/
        if($conn->query($beacon_rssi_sql) === true) {
            echo "isnert successful <br/>";
        } else {
            echo "Error: " . $beacon_rssi_sql . "<br>" . $conn->error . "<br/>";
        }
    }

    // RSSI登録
    function create_rssi($beacon_id, $micro_id, $rssi, $date_time) {
        // グローバル変数を定義
        GLOBAL $conn;
        $rssi_sql = "INSERT INTO my_rssi (id, beacon_id, micro_id, rssi, date_time)
                    VALUES (null, '$beacon_id', '$micro_id', '$rssi', '$date_time')";
        if($conn->query($rssi_sql) === true) {
            echo "isnert successful <br/>";
        } else {
            echo "Error: " . $rssi_sql . "<br>" . $conn->error . "<br/>";
        }
    }

?>