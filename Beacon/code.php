<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ビーコン</title>
	<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/square.css">
	<script src="../bootstrap/js/jquery-3.4.1.js"></script>
	<script>
		$(function(){
			$('.container-box, .text_name').hide();
			$('#text_name, #figure_length, #figure_width').focus(function(){}).focusout(function(){
				var text_name     = $('#text_name').val();
				var figure_length = $('#figure_length').val()*2;
				var figure_width  = $('#figure_width').val()*2;
        		if(figure_length > 500) {
					$('.container-box').height(500);
				} else if(figure_width > 500) {
					$('.container-box').width(500);
				} else {
					$('.container-box').width($('#figure_length').val()*2);	
					$('.container-box').height($('#figure_width').val()*2);	
				}
				if(text_name != "") {
					$('.text_name').text($('#text_name').val() + "様の部屋はこんな感じですねぇ～");
				}
				$('.container-box, .text_name').show();
    		});
		});
	</script>
</head>
<body>
	<div class="container">
		<div class="header">登録</div>
		<div class="row">
			<div class="content-left col-md-6">
				<form method="POST" action="create_room.php">
				  	<div class="form-row">
					    <div class="form-group col-md-10">
					      	<label>名前</label>
					      	<input type="text" class="form-control" name="room_name" placeholder="名前" id="text_name" required="">
					    </div>
					</div>
					<div class="form-row">
					    <div class="form-group col-md-10">
						    <label>部屋の長</label>
						    <input type="number" class="form-control" name="length_of_room" placeholder="メートル" id="figure_length" required="">
					    </div>
					</div>
					<div class="form-row">
					    <div class="form-group col-md-10">
						    <label>部屋の幅</label>
						    <input type="number" class="form-control" name="width_of_room" placeholder="メートル" id="figure_width" required="">
					    </div>
					</div>
					<div class="form-row">
					    <div class="form-group col-md-10">
						    <label>部屋の高</label>
						    <input type="number" class="form-control" name="height_of_room" placeholder="メートル" id="figure_height" required="">
					    </div>
				  	</div>
				    <div class="form-row">
					    <div class="form-group col-md-10">
					      	<label>ビーコンの名前</label>
					      	<input type="text" class="form-control" name="beacon_name">
					    </div>
					</div>
					<div class="form-row">
					    <div class="form-group col-md-10">
					      	<label>ビーコンと部屋の選択</label><br>
					     	<select name="select_beacon_id" class="col-md-5">
								<option name="beacon">ビーコン名</option>
								<?php 
									$conn = new mysqli("localhost", "root", "leduc", "ibeacon_tbl");
									$beacon_room = "SELECT * FROM my_beacon_room_relation";
									$my_beacon = "SELECT * FROM my_beacon";
									$data_beacon_room = mysqli_query($conn, $beacon_room);
									$data_my_beacon = mysqli_query($conn, $my_beacon);
									while($row = mysqli_fetch_assoc($data_my_beacon)) {
								?>
										<option value="<?php echo $row["id"]?>"><?php echo $row["beacon_name"]?></option>
										
								<?php	}	?>
							</select>
							<select name="select_room_id"  class="col-md-5">
								<option>部屋名</option>
								<?php 
									$conn = new mysqli("localhost", "root", "leduc", "ibeacon_tbl");
									$beacon_room = "SELECT * FROM my_beacon_room_relation";
									$my_room = "SELECT * FROM my_room";
									$data_beacon_room = mysqli_query($conn, $beacon_room);
									$data_my_room = mysqli_query($conn, $my_room);
									while($row = mysqli_fetch_assoc($data_my_room)) {
								?>
										<option value="<?php echo $row["id"]?>"><?php echo $row["room_name"]?></option>
										
								<?php	}	?>
							</select>
					    </div>
					</div><br>
				  <button type="submit" class="btn btn-primary col-md-3" id="submit" name="btn_submit">登録</button>
				</form>
			</div>
			<div class="content-right col-md-6">
				<div class="text_name"></div>
				<div class="container-box">
					<div class="cube">
						<div class="box font"></div>
						<div class="box back"></div>
						<div class="box left"></div>
						<div class="box right"></div>
						<div class="box top"></div>
						<div class="box bottom"></div>
						<div class="center"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- <script src="../js/home.js" type="text/javascript"></script> -->
</body>
</html>